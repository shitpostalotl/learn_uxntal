[Guide](https://comudanzas.net/uxn_tutorial_day_2.html) "system device and colors"

[download (aur)](https://aur.archlinux.org/packages/uxn-git) had to remove the build.sh.patch to get it to build, wtf?

[visual notes](https://nchrs.xyz/site/uxn_notes.html)

# NOTES DAY 1:
|0100: initial value of the program counter. whatever comes next, it comes at this adr

LIT: push the next byte in memory onto the stack, then make the program counter skip that byte

NON-18 #S: https://wiki.xxiivv.com/site/ascii.html

18: i/o adress of device 1 port 8: console, write

DEO: Output a byte from stack to the given device address

---

|: location where the next written items will be in memory. 1-byte: i/o memory or zero page. 2-byte: main memory

`#`: shorthand for LIT

': read ascii character's numerical value. cant use w/ # and doesn't add a LIT itself )

@: assign a label to current adress

&: assign sub-labels

$: relative pads (skip bytes)

[]: ignored. used for readability

%: define a macro (custom word)

{}: set the definition of a macro

---

i may code in this with capslock on lmao

# NOTES DAY 2:

